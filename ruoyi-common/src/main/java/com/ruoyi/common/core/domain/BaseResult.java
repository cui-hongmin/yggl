package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.utils.StringUtils;

import java.io.Serializable;
import java.util.List;

import static com.ruoyi.common.core.domain.AjaxResult.DATA_TAG;
import static com.ruoyi.common.core.domain.AjaxResult.MSG_TAG;


/**
 * <p>Tile:控制层消息返回基类</p>
 */
public class BaseResult<T>  implements Serializable {

    /** 序列号 */
    private static final long serialVersionUID = -6274137768408671754L;

    /** 是否成功标识 */
    protected boolean success = false;

    /** 状态码 */
    protected Integer code;

    /** 返回内容 */
    protected String msg;

    /** 错误时的错误代码 */
    protected String errorCode;

    /** 错误时的错误信息 */
    protected String errorMsg;

    /** 返回的结果数据 */
    protected  T data;

    /** 每页显示行数 */
    protected Integer pageSize = 10;

    /** 当前页数 */
    protected Object pageNum = 1;

    /** 总记录数 */
    protected Object total;

    /** pc为了保持长效链接，前端当用户登陆后访问每个接口都应该带上uid和token，然后后台当方法执行结束后，应该重新设置token返回给前端的值*/
    protected String token;

    private Integer totalPage=0;

    private final static String ERRORCODE="500";

    public BaseResult(){}

    public BaseResult(boolean success) {
        super();
        this.success = success;
    }

    public BaseResult(boolean success,T data) {
        super();
        this.success = success;
        this.data=data;
    }

    public BaseResult(boolean success, T data, Integer pageSize, Integer pageNum,
                      Integer total) {
        super();
        this.success = success;
        this.data = data;
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.total = total;
    }
    /**
     * 直接设置错误信息，错误代码默认600
     * @param errorMsg
     */
    public BaseResult(String errorMsg) {
        super();
        this.errorCode = BaseResult.ERRORCODE;
        this.errorMsg = errorMsg;
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    public BaseResult(int code, String msg, T data)
    {
        super();
        this.code = code;
        this.msg = msg;
        if (StringUtils.isNotNull(data))
        {
            this.data = data;
        }
    }

    /**
     * 自己设置错误代码和错误信息
     * @param errorCode
     * @param errorMsg
     */
    public BaseResult(String errorCode,String errorMsg) {
        super();
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
        if(data instanceof List && pageSize ==null) {
            this.pageSize = 10;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Object getTotal() {
        return total;
    }

    public void setTotal(Object total) {
        this.total = total;
    }

    /** 重写toString */
    @Override
    public String toString() {
        return String.format("Result [errorCode=%s, errorMsg=%s, object=%s]",errorCode, errorMsg, data);
    }

    /**
     *
     * <p>Description:失败处理</p>
     * @date 2016年10月25日 下午12:56:29
     * @author 吴轶雷
     * @param errorCode 错误代码
     * @param errorMsg 错误信息
     * @return
     */
    public static <T> BaseResult<T> fail(String errorCode,String errorMsg){
        return new BaseResult<T>(errorCode,errorMsg);
    }

    /**
     *
     * <p>Description:成功处理</p>
     * @date 2016年10月25日 下午2:30:16
     * @author
     * @param data
     * @return
     */
//    public static <T> BaseResult<T> success(T data){
//        return new BaseResult<T>(true,data);
//    }
//
//    public static <T> BaseResult<T> success(){
//        return new BaseResult<T>(true,null);
//    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static BaseResult success()
    {
        return BaseResult.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static BaseResult success(Object data)
    {
        return BaseResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static BaseResult success(String msg)
    {
        return BaseResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static BaseResult success(String msg, Object data)
    {
        return new BaseResult(HttpStatus.SUCCESS, msg, data);
    }




    public Integer getTotalPage() {
        if(null!=total && !total.toString().equals("0")&& null!=pageSize && !pageSize.toString().equals("0")){
            Integer r = Integer.parseInt(pageSize.toString());
            Integer t = Integer.parseInt(total.toString());
            if(t%r==0){
                totalPage = t/r;
            }else{
                totalPage = t/r+1;
            }
        }
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
