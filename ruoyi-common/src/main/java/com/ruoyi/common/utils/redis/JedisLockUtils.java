package com.ruoyi.common.utils.redis;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

import java.util.Collections;

/**
 * jedis分布式锁工具类
 * @autho ConnorSong
 * @date 2021/1/20 6:26 下午
 */
@Slf4j
public class JedisLockUtils {

    private static final String LOCK_SUCCESS = "OK";
    private static final String SET_IF_NOT_EXIST = "NX";
    private static final String SET_WITH_EXPIRE_TIME = "PX";

    private static final Long RELEASE_SUCCESS = 1L;
    /**
     * 尝试获取分布式锁
     * @param jedis Redis客户端
     * @param lockKey 锁
     * @param lockValue value
     * @param expireTime 超期时间(秒)
     * @return 是否获取成功
     */
    public static boolean tryGetLock(Jedis jedis, String lockKey, String lockValue, int expireTime) {
        log.error("----获取Jedis分布式锁----lockKey:{}", lockKey);

        try {
            //方案一，具有原子性，并且可以设置过期时间，避免拿到锁后，业务代码出现异常，无法释放锁
            String result = jedis.set(lockKey, lockValue, new SetParams().nx().ex(expireTime));
//            String result = jedis.set(lockKey, lockValue, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);
            if (LOCK_SUCCESS.equals(result)) {
                return true;
            }
            return false;
            //方案二，setnx()具有原子性，但是有后续判断，整体不具有原子性，不能设置过期时间
//      //setnx(lockkey, 当前时间+过期超时时间)，如果返回 1，则获取锁成功；如果返回 0 则没有获取到锁
//      String value = new Date().getTime() + expireTime + "";
//      if(1 == jedis.setnx(lockKey, value)){
//        return true;
//      }else{
//        String oldExpireTime = jedis.get(lockKey);
//        if(Long.valueOf(oldExpireTime)< new Date().getTime()){
//          //锁超时，可以获取锁重新设置锁
//          //计算 newExpireTime = 当前时间+过期超时时间，然后 getset(lockkey, newExpireTime) 会返回当前 lockkey的值currentExpireTime
//          long newExpireTime = new Date().getTime() + expireTime;
//          String currentExpireTime = jedis.getSet(lockKey, newExpireTime + "");
//          if(currentExpireTime.equals(oldExpireTime)){
//            return true;
//          }
//        }
//        return false;
//      }
        }finally {
            returnResource(jedis);
        }
    }

    /**
     * 释放分布式锁
     * @param jedis Redis客户端
     * @param lockKey 锁
     * @return 是否释放成功
     */
    public static boolean closeLock(Jedis jedis, String lockKey, String lockValue) {
        log.error("----释放Jedis分布式锁----lockKey:{}, lockValue:{}", lockKey, lockValue);
        try {
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Object result = jedis.eval(script, Collections.singletonList(lockKey), Collections.singletonList(lockValue));
            if (RELEASE_SUCCESS.equals(result)) {
                return true;
            }
            return false;
        }finally {
            returnResource(jedis);
        }
    }
    /**
     * 关闭资源
     * @param jedis
     */
    public static void returnResource(final Jedis jedis){
        if(null != jedis){
            jedis.close();
        }
    }
}
