package com.ruoyi.common.utils.face.dto;

import com.ruoyi.common.utils.face.constant.ImageTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 图像对象
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImageU {
    private ImageTypeEnum imageTypeEnum;

    private String data;
}
