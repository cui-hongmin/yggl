package com.ruoyi.common.redission.admin;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
//@AllArgsConstructor
public class ProductSkuSupplierInfoDTO implements Serializable {

//    private Integer id;

    private String sku;

    private String supplierName;

    private String supplierCode;

    private String supplierUrl;
}
