package com.ruoyi.common.redission.service.impl;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.redission.admin.ILock;
import com.ruoyi.common.redission.admin.ProductSkuSupplierInfoDTO;
import com.ruoyi.common.redission.service.IDistributedLock;
import com.ruoyi.common.redission.service.IProductSkuSupplierMeasureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class ProductSkuSupplierMeasureServiceImpl implements IProductSkuSupplierMeasureService {


    @Resource
    private IDistributedLock distributedLock;

    /**
     * 手动释放锁示例
     * */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveSupplierInfo(ProductSkuSupplierInfoDTO dto) {
        // 手动释放锁
        String sku = dto.getSku();
        ILock lock = null;
        try {
            lock = distributedLock.lock(dto.getSku(),10L, TimeUnit.SECONDS, false);
            if (Objects.isNull(lock)) {
                throw new ServiceException("Duplicate request for method still in process");
//                throw new BusinessException("Duplicate request for method still in process");
            }
            // 业务代码
        }catch (ServiceException e) {
            throw new ServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("保存异常", e);
            throw new ServiceException (e.getMessage());
        } finally {
            if (Objects.nonNull(lock)) {
                distributedLock.unLock(lock);
            }
        }
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean editSupplierInfo(ProductSkuSupplierInfoDTO dto) {
        String sku = dto.getSku();
        // try-with-resources 语法糖自动释放锁
        try(ILock lock = distributedLock.lock(dto.getSku(),10L, TimeUnit.SECONDS, false)) {
            if(Objects.isNull(lock)){
                throw new ServiceException ("Duplicate request for method still in process");
            }

            System.out.println("执行操作");

            // 业务代码
        }catch (ServiceException e) {
            throw new ServiceException (e.getMessage());
        } catch (Exception e) {
            log.error("修改异常", e);
            throw new ServiceException ("修改异常");
        }
        return Boolean.TRUE;

    }
}
