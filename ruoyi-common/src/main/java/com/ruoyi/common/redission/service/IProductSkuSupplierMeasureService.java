package com.ruoyi.common.redission.service;

import com.ruoyi.common.redission.admin.ProductSkuSupplierInfoDTO;

/**
 * 注入IDistributedLock接口使用示例
 */

// 定义接口
public interface IProductSkuSupplierMeasureService {

    /**
     * 保存SKU供应商供货信息
     *
     * @param dto
     * @return
     */
    Boolean saveSupplierInfo(ProductSkuSupplierInfoDTO dto);
    /**
     * 编辑SKU供应商供货信息
     *
     * @param dto
     * @return
     */
    Boolean editSupplierInfo(ProductSkuSupplierInfoDTO dto);


}
