package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseResult;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.YgSalaryVo;
import com.ruoyi.system.domain.bo.YgSalaryBo;
import com.ruoyi.system.domain.YgSalary;
import com.ruoyi.system.service.IYgSalaryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/salary")
public class YgSalaryController extends BaseController {

    private final IYgSalaryService iYgSalaryService;

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:salary:list")
    @GetMapping("/list")
    public TableDataInfo<YgSalaryVo> list(YgSalaryBo bo, PageQuery pageQuery) {
        return iYgSalaryService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @SaCheckPermission("system:salary:list")
    @GetMapping("/list1")
    public BaseResult list1(YgSalaryBo bo, PageQuery pageQuery) {
        return iYgSalaryService.selectSalaryList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:salary:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(YgSalaryBo bo, HttpServletResponse response) {
        List<YgSalaryVo> list = iYgSalaryService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", YgSalaryVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:salary:query")
    @GetMapping("/{id}")
    public R<YgSalary> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Integer id) {
        return R.ok(iYgSalaryService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:salary:add")
    @PostMapping()
    public AjaxResult add(@RequestBody YgSalary ygSalary) {
        return iYgSalaryService.insertByBo(ygSalary);
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:salary:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody YgSalaryBo bo) {
        return toAjax(iYgSalaryService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:salary:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Integer[] ids) {
        return toAjax(iYgSalaryService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
