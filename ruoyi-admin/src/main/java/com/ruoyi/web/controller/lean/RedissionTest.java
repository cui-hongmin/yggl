package com.ruoyi.web.controller.lean;


import cn.dev33.satoken.annotation.SaIgnore;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.redission.admin.ProductSkuSupplierInfoDTO;
import com.ruoyi.common.redission.annotation.DistributedLock;

import com.ruoyi.common.redission.service.IProductSkuSupplierMeasureService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SaIgnore
@RestController
@RequestMapping("/redission/lock")
public class RedissionTest {

    @Autowired
    private IProductSkuSupplierMeasureService productSkuSupplierMeasureService;



    @PostMapping("/editSupplierInfo")
//    @DistributedLock(key = "#dto.sku + '-' + #dto.skuId", lockTime = 10L, keyPrefix = "sku-")
    @DistributedLock(key = "#dto.sku", lockTime = 10L, keyPrefix = "sku-")
    public R<Boolean> editSupplierInfo(@RequestBody @Validated ProductSkuSupplierInfoDTO dto) {
        return R.ok(productSkuSupplierMeasureService.editSupplierInfo(dto));
    }


    @PostMapping("/editSupp")
//    @DistributedLock(key = "#dto.sku + '-' + #dto.skuId", lockTime = 10L, keyPrefix = "sku-")
//    @DistributedLock(key = "#dto.sku", lockTime = 10L, keyPrefix = "sku-")
    public AjaxResult editSupplier(@RequestBody ProductSkuSupplierInfoDTO dto) {
        System.out.println(dto.getSku());

        return AjaxResult.success("成功le");
    }


}
