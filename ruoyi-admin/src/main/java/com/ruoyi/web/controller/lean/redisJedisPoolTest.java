package com.ruoyi.web.controller.lean;

import com.ruoyi.common.utils.redis.JedisLockUtils;
import com.ruoyi.common.utils.redis.RedisTool;
import redis.clients.jedis.Jedis;

public class redisJedisPoolTest {
    public static void main(String[] args) {
        redisJedisPoolTest redisJedisPoolTest = new redisJedisPoolTest();
        int i = redisJedisPoolTest.updateWaresInventory("1", "10");
        System.out.println(i);
    }


    public int updateWaresInventory(String id,  String purchase) {
        int result=0;
        System.err.println("=============线程开启============" + Thread.currentThread().getName());
        Jedis jedis = new Jedis();
        try {
            // 获取锁
            boolean redisKey = JedisLockUtils.tryGetLock(jedis, "redisKey"+id, purchase, 1000);
            if(redisKey){
                try{
                    System.err.println("=============获取锁，开始执行============" + Thread.currentThread().getName());
                    System.out.println("处理业务逻辑");
                    result = 1;
                    return result;
                }finally {
                    System.err.println("=============操作完毕,释放锁============" + Thread.currentThread().getName());
                    // 释放锁
                    boolean b = JedisLockUtils.closeLock(jedis, "redisKey" + id, purchase);
                    if (b) {
                        System.out.println("释放锁成功：" + b);
                    }else {
                        System.out.println("释放锁失败：" + b);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("未获取到锁");
        return result;
    }
}
