package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.FaceResultUtil;
import com.ruoyi.common.utils.FaceUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.aliyun.AliyunOSSUtil;
import com.ruoyi.common.utils.face.Base64DecodeMultipartFile;
import com.ruoyi.common.utils.face.FaceManage;
import com.ruoyi.common.utils.face.GsonUtils;
import com.ruoyi.common.utils.face.HttpUtil;
import com.ruoyi.common.utils.face.constant.FaceConstant;
import com.ruoyi.common.utils.face.constant.ImageTypeEnum;
import com.ruoyi.common.utils.face.dto.FaceResult;
import com.ruoyi.common.utils.face.dto.FaceUserDTO;
import com.ruoyi.common.utils.face.dto.ImageU;
import com.ruoyi.common.utils.face.exce.BizException;
//import com.ruoyi.system.service.IStudentService;
import com.ruoyi.system.domain.WorkAttendance;
import com.ruoyi.system.service.ISysLogininforService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.IWorkAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Descripttion
 * @Author cuihongmin
 * @Date 2023/12/7 10:53
 */

@RestController
@RequestMapping("/system/face")
public class SysFaceController {

    private static final String APP_ID = "35645392";
    private static final String APP_KEY = "UqXC1Lboi4M0rI3I8BevnRXk";
    private static final String SECRET_KEY = "eTAdGscGn24tZtYF6fC8dwX23Lzp9HWB";

    //OSS服务器访问域名
    @Value("${aliyun.oss.endpoint}")
    private String endpoint;

    //子账户名
    @Value("${aliyun.oss.accessKeyId}")
    private String accessKeyId;

    //子账户密码
    @Value("${aliyun.oss.accessKeySecret}")
    private String accessKeySecret;

    //桶名字
    @Value("${aliyun.oss.bucketName}")
    private String bucketName;


    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysLogininforService sysLogininforService;
    @Autowired
    private IWorkAttendanceService workAttendanceService;

    /**
     * 人脸注册
     */
    @PostMapping("/faceRegister")
    public AjaxResult faceRegister(@RequestParam("file") String file, @RequestParam("groupId") String groupId) throws Exception{
        MultipartFile multipartFile = Base64DecodeMultipartFile.base64Convert(file);//很长
        if (multipartFile.isEmpty()) {
            throw new BizException("上传图片不能为空");
        }
        String data = FaceUtil.encodeBase64(multipartFile.getBytes());
        ImageU imageU = ImageU.builder().data(data).imageTypeEnum(ImageTypeEnum.BASE64).build();

        FaceUserDTO<String> userDTO = new FaceUserDTO<>();
        userDTO.setGroupId("group1");
        userDTO.setUserId(groupId);
//        String image = "https://download.2dfire.com/mis/permanent/img2.jpg";
//        ImageU imageU = ImageU.builder().data(file).imageTypeEnum(ImageTypeEnum.URL).build();
        userDTO.setUser("用户信息1");
        FaceManage.faceRegister(userDTO, imageU);
        return AjaxResult.success("人脸注册成功");
    }


    /**
     * 进入宿舍人脸识别
     * @param file      导入文件
     * @param groupId   用户登录账号名称
     */
    @PostMapping("/facelogin")
//    @ResponseBody
    public AjaxResult facelogin(@RequestParam("file") String file, @RequestParam("groupId") String groupId) throws Exception {

        /**
         * base64转为multipartFile
         */
        MultipartFile multipartFile = Base64DecodeMultipartFile.base64Convert(file);//很长
        if (multipartFile.isEmpty()) {
            throw new BizException("未识别到人脸");
        }

//        String groupIds = "cui123";
        String data = FaceUtil.encodeBase64(multipartFile.getBytes());
        ImageU imageU = ImageU.builder().data(data).imageTypeEnum(ImageTypeEnum.BASE64).build();
        System.out.println("123");
        System.out.println(imageU);
//        String image = "https://download.2dfire.com/mis/permanent/img1.jpg";
//        ImageU imageU = ImageU.builder().data(image).imageTypeEnum(ImageTypeEnum.URL).build();
        String url = "https://aip.baidubce.com/rest/2.0/face/v3/search";

        try {
        Map<String, Object> map = new HashMap<>();
        map.put("image", imageU.getData());
        map.put("liveness_control", "NORMAL");
        map.put("group_id_list", "group1");
        map.put("image_type", imageU.getImageTypeEnum().name());
        map.put("quality_control", "LOW");
        map.put("user_id",groupId);
        String param = GsonUtils.toJson(map);

        // 获取access_token
        String accessToken = getAuth(APP_KEY, SECRET_KEY);
        String result = HttpUtil.post(url, accessToken, "application/json", param);
            org.json.JSONObject myJsonObject =  new  org.json.JSONObject(result);
            FaceResult success = FaceResultUtil.isSuccess(myJsonObject);
            String users = success.getData().getString(FaceConstant.USER_LIST);
            if (StringUtils.isEmpty(users)) {
            return AjaxResult.error("该用户没有注册人脸，请注册人脸");
            }
            JSONArray array = JSONObject.parseArray(users);
            JSONObject object = JSONObject.parseObject(array.get(0).toString());
            Integer score = object.getInteger(FaceConstant.SCORE);
            System.out.println("kkkkk");
            System.out.println(score);
            if (score < 90) {
            return AjaxResult.error("该人脸不符");
           }
            System.out.println(result);
            if (score >= FaceConstant.MATCH_SCORE) {
            AjaxResult ajax = AjaxResult.success();

            System.out.println(success.getData().toString());
            String userId = object.getString("user_id");
            // 根据用户账号查询用户信息
            SysUser sysUser = sysUserService.selectUserByUserName(userId);
            WorkAttendance workAttendance = new WorkAttendance();
            workAttendance.setUserId(sysUser.getUserId());
            workAttendance.setEmployeeNumber(sysUser.getYgNumber()); // 员工编号
            workAttendance.setYgName(sysUser.getName());
            workAttendance.setClockIn(new Date()); // 上班时间
            int i = workAttendanceService.insertUser(workAttendance);
            if (i == 3) {
                AjaxResult.error(501,"今日已成功打卡,请勿重复打卡!");
            } else if (i == 0) {
                AjaxResult.error(501,"系统错误,请联系管理员处理!");
            }

            return AjaxResult.success("上班打卡成功");

        }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return AjaxResult.error("该用户没有注册人脸，请注册人脸");


    }

    /**
     * 出宿舍人脸识别
     */
    @PostMapping("/faceLoginCs")
//    @ResponseBody
    public AjaxResult faceLoginCs(@RequestParam("file") String file, @RequestParam("groupId") String groupId) throws Exception {

        /**
         * base64转为multipartFile
         */
        MultipartFile multipartFile = Base64DecodeMultipartFile.base64Convert(file);//很长
        if (multipartFile.isEmpty()) {
            throw new BizException("上传文件不能为空");
        }

        String groupIds = "cui123";
        String data = FaceUtil.encodeBase64(multipartFile.getBytes());
        ImageU imageU = ImageU.builder().data(data).imageTypeEnum(ImageTypeEnum.BASE64).build();
        System.out.println("123");
        System.out.println(imageU);
//        String image = "https://download.2dfire.com/mis/permanent/img1.jpg";
//        ImageU imageU = ImageU.builder().data(image).imageTypeEnum(ImageTypeEnum.URL).build();
        String url = "https://aip.baidubce.com/rest/2.0/face/v3/search";

        try {
            Map<String, Object> map = new HashMap<>();
            map.put("image", imageU.getData());
            map.put("liveness_control", "NORMAL");
            map.put("group_id_list", "group1");
            map.put("image_type", imageU.getImageTypeEnum().name());
            map.put("quality_control", "LOW");
            map.put("user_id",groupId);
            String param = GsonUtils.toJson(map);

            // 获取access_token
            String accessToken = getAuth(APP_KEY, SECRET_KEY);
            String result = HttpUtil.post(url, accessToken, "application/json", param);
            org.json.JSONObject myJsonObject =  new  org.json.JSONObject(result);
            FaceResult success = FaceResultUtil.isSuccess(myJsonObject);
            String users = success.getData().getString(FaceConstant.USER_LIST);
            if (StringUtils.isEmpty(users)) {
                return AjaxResult.error("该用户没有注册人脸，请注册人脸");
            }
            JSONArray array = JSONObject.parseArray(users);
            JSONObject object = JSONObject.parseObject(array.get(0).toString());
            Integer score = object.getInteger(FaceConstant.SCORE);
            System.out.println("kkkkk");
            System.out.println(score);
            if (score < 90) {
                return AjaxResult.error("该人脸不符");
            }
            System.out.println(result);
            if (score >= FaceConstant.MATCH_SCORE) {
                AjaxResult ajax = AjaxResult.success();

                System.out.println(success.getData().toString());
                String userId = object.getString("user_id");
                // 根据用户账号查询用户信息
                SysUser sysUser = sysUserService.selectUserByUserName(userId);
                WorkAttendance workAttendance = new WorkAttendance();
                workAttendance.setUserId(sysUser.getUserId());
//                workAttendance.setLockOut(new Date());
                int i = workAttendanceService.updateWorkattendance(workAttendance);
                if (i == 4) {
                    AjaxResult.error(501,"当日未上班打卡!");
                } else if (i == 0) {
                    AjaxResult.error(501,"系统错误,请联系管理员处理!");
                }

                return AjaxResult.success("下班打卡成功");

                // 根据学号改变学生状态
//                studentService.updateStatusById2(userId,"2");

//            SysUser sysUser = sysUserService.selectUserById(user_id);
//            long userId = 1;
//            SysUser sysUser = sysUserService.selectUserById(userId);
//            SysLogininfor sysLogininfor = new SysLogininfor();
//            sysLogininfor.setUserName(sysUser.getUserName());
//            List<SysLogininfor> sysLogininfors = sysLogininforService.selectLogininforList(sysLogininfor);
//            ajax.put("user", sysLogininfors);
//                return AjaxResult.success("人脸识别成功");

            }
//        return AjaxResult.error();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return AjaxResult.error("该用户没有注册人脸，请注册人脸");


    }



    @PostMapping("/upload3")
    public String upload3(@RequestParam("file") MultipartFile file) throws Exception {
        String url = AliyunOSSUtil.uploadImage(endpoint, accessKeyId, accessKeySecret, bucketName, file.getOriginalFilename(), file.getBytes());
        System.out.println("===千锋健哥===" + url);
        return url;
    }

    /**
     * 获取API访问token
     * 该token有一定的有效期，需要自行管理，当失效时需重新获取.
     * @param ak - 百度云官网获取的 API Key
     * @param sk - 百度云官网获取的 Securet Key
     * @return assess_token 示例：
     * "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567"
     */
    public static String getAuth(String ak, String sk) {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
            // 1. grant_type为固定参数
            + "grant_type=client_credentials"
            // 2. 官网获取的 API Key
            + "&client_id=" + ak
            // 3. 官网获取的 Secret Key
            + "&client_secret=" + sk;
        try {
            URL realUrl = new URL(getAccessTokenUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.err.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            /**
             * 返回结果示例
             */
            System.err.println("result:" + result);
            org.json.JSONObject jsonObject = new org.json.JSONObject(result);
            String access_token = jsonObject.getString("access_token");
            return access_token;
        } catch (Exception e) {
            System.err.printf("获取token失败！");
            e.printStackTrace(System.err);
        }
        return null;
    }

}
