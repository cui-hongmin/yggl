package com.ruoyi.test;


import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.WorkAttendance;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.IWorkAttendanceService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;


@SpringBootTest
@DisplayName("打卡功能测试")
public class WorkTest {
    @Autowired
    private IWorkAttendanceService workAttendanceService;

    @Autowired
    private ISysUserService sysUserService;


    @Test
    public void getIdTest() throws Exception {

        SysUser sysUser = sysUserService.selectUserByUserName("admin");
        WorkAttendance workAttendance = new WorkAttendance();
        workAttendance.setUserId(sysUser.getUserId());
        workAttendance.setEmployeeNumber(sysUser.getYgNumber()); // 员工编号
        workAttendance.setYgName(sysUser.getName());
        workAttendance.setClockIn(new Date()); // 上班时间
        int i = workAttendanceService.insertUser(workAttendance);
        System.out.println(i);

    }
}
