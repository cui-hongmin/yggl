package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】业务对象 yg_salary
 *
 * @author ruoyi
 * @date 2024-01-09
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class YgSalaryBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Integer id;

    /**
     * 员工编号
     */
    @NotBlank(message = "员工编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ygNumber;

    /**
     * 员工姓名
     */
    @NotBlank(message = "员工姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ygName;

    /**
     * 岗位id
     */

    private String ygPostId;

    /**
     * 用户id
     */

    private Long userId;

    /**
     * 年月
     */
//    @JsonFormat(pattern = "yyyy-MM")
    private String ygTime;

    /**
     * 基本工资
     */
    @NotNull(message = "基本工资不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal basicSalary;

    /**
     * 职位津贴
     */

    private String postAllowance;

    /**
     * 员工工龄
     */

    private String ygSeniority;

    /**
     * 扣款
     */

    private BigDecimal deduction;

    /**
     * 实际工资
     */

    private BigDecimal realWages;

    /**
     * 下发状态(0：未下发；1：下发)
     */

    private String status;


    /**
     * 备注
     */

    private String remark;


}
