package com.ruoyi.system.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("work_attendance")
public class WorkAttendance extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * userId
     */
    private Long userId;

    /**
     * 员工编号
     */
    private String employeeNumber;

    /**
     * 用户姓名
     */
    private String ygName;

    /**
     * 上班时间
     */
    private Date clockIn;

    /**
     * 下班时间
     */
    private Date lockOut;

    /**
     * 工作时长
     */
    private String workHours;

    /**
     * 备注
     */
    private String remark;


}
