package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 yg_salary
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("yg_salary")
public class YgSalary extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 员工编号
     */
    private String ygNumber;
    /**
     * 员工姓名
     */
    private String ygName;
    /**
     * 岗位id
     */
    private String ygPostId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 年月
     */
//    @JsonFormat(pattern = "yyyy-MM")
    private String ygTime;
    /**
     * 基本工资
     */
    private BigDecimal basicSalary;
    /**
     * 职位津贴
     */
    private String postAllowance;
    /**
     * 员工工龄
     */
    private Long ygSeniority;

    private String ygSeniorityStr;

    /**
     * 工龄奖金
     * */
    private BigDecimal ygSenioiityMoney;
    /**
     * 扣款
     */
    private BigDecimal deduction;
    /**
     * 实际工资
     */
    private BigDecimal realWages;
    /**
     * 下发状态(0：未下发；1：下发)
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户创建时间
     * */
    private Date userDate;

    public String getYgSeniorityStr() {
        if(this.ygSeniority != null){
            if(this.ygSeniority < 12){
                ygSeniorityStr = "未满1年";
            } else if(12 <= this.ygSeniority &&  this.ygSeniority < 24 ){
                ygSeniorityStr = "1年";
            } else if(24 <= this.ygSeniority &&  this.ygSeniority < 36){
                ygSeniorityStr = "2年";
            } else if(36 <= this.ygSeniority ){
                ygSeniorityStr = "3年以上";
            }
        }
        return ygSeniorityStr;
    }

    public void setYgSeniorityStr(String ygSeniorityStr) {
        this.ygSeniorityStr = ygSeniorityStr;
    }
}
