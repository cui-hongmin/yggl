package com.ruoyi.system.domain.vo;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 【请填写功能名称】视图对象 yg_salary
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@Data
@ExcelIgnoreUnannotated
public class YgSalaryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Integer id;

    /**
     * 员工编号
     */
    @ExcelProperty(value = "员工编号")
    private String ygNumber;

    /**
     * 员工姓名
     */
    @ExcelProperty(value = "员工姓名")
    private String ygName;

    /**
     * 岗位id
     */
    @ExcelProperty(value = "岗位id")
    private String ygPostId;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long userId;

    /**
     * 基本工资
     */
    @ExcelProperty(value = "基本工资")
    private BigDecimal basicSalary;

    /**
     * 职位津贴
     */
    @ExcelProperty(value = "职位津贴")
    private String postAllowance;

    /**
     * 员工工龄
     */
    @ExcelProperty(value = "员工工龄")
    private String ygSeniority;

    /**
     * 扣款
     */
    @ExcelProperty(value = "扣款")
    private BigDecimal deduction;

    /**
     * 实际工资
     */
    @ExcelProperty(value = "实际工资")
    private BigDecimal realWages;

    /**
     * 下发状态(0：未下发；1：下发)
     */
    @ExcelProperty(value = "下发状态(0：未下发；1：下发)")
    private String status;


    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
