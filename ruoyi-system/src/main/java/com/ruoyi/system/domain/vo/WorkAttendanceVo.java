package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
@ExcelIgnoreUnannotated
public class WorkAttendanceVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Integer id;

    /**
     * userId
     */
    private Long userId;

    /**
     * 员工编号
     */
    @ExcelProperty(value = "员工编号")
    private String employeeNumber;

    /**
     * 用户姓名
     */
    @ExcelProperty(value = "用户姓名")
    private String ygName;

    /**
     * 上班时间
     */
    @ExcelProperty(value = "上班时间")
    private Date clockIn;

    /**
     * 下班时间
     */
    @ExcelProperty(value = "下班时间")
    private Date lockOut;

    /**
     * 工作时长
     */
    @ExcelProperty(value = "工作时长")
    private String workHours;

    /**
     * 备注
     */
    private String remark;
}
