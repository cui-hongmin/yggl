package com.ruoyi.system.service;


import com.ruoyi.system.domain.WorkAttendance;

/**
 * 用户打卡 业务层
 *
 * @author
 */
public interface IWorkAttendanceService {

    /**
     * 新增用户上班打卡信息
     *
     * @param workAttendance 用户打卡信息
     * @return 结果
     */
    int insertUser(WorkAttendance workAttendance);

    int updateWorkattendance(WorkAttendance workAttendance);
}
