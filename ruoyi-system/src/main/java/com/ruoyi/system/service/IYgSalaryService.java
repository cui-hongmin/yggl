package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseResult;
import com.ruoyi.system.domain.YgSalary;
import com.ruoyi.system.domain.vo.YgSalaryVo;
import com.ruoyi.system.domain.bo.YgSalaryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public interface IYgSalaryService {

    /**
     * 查询【请填写功能名称】
     */
    YgSalary queryById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<YgSalaryVo> queryPageList(YgSalaryBo bo, PageQuery pageQuery);

    /**
     * 查询薪资列表
     */
    BaseResult selectSalaryList(YgSalaryBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<YgSalaryVo> queryList(YgSalaryBo bo);

    /**
     * 新增【请填写功能名称】
     */
    AjaxResult insertByBo(YgSalary ygSalary);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(YgSalaryBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid);
}
