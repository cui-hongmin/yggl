package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.system.domain.WorkAttendance;

import com.ruoyi.system.domain.YgSalary;
import com.ruoyi.system.mapper.WorkAttendanceMapper;
import com.ruoyi.system.service.IWorkAttendanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@Service
public class WorkAttendanceServiceImpl implements IWorkAttendanceService {

    private final WorkAttendanceMapper baseMapper;

    @Override
    public int insertUser(WorkAttendance workAttendance) {
        // 先查询该员工当日有没有打卡
        WorkAttendance work = baseMapper.selectWorkDay(workAttendance.getUserId());
        if (work != null) {
            // 当日已经打过卡了
            return 3;
        }
        int rows = baseMapper.insert(workAttendance);
//         baseMapper.insertWorkAttendance(workAttendance);

        return rows;
    }

    @Override
    public int updateWorkattendance(WorkAttendance workAttendance) {
        // 根据userId查询该用户当日有没有上班打卡?
        WorkAttendance work = baseMapper.selectWorkDay(workAttendance.getUserId());
        if (work == null) {
            // 当日未上班打卡
            return 4;
        }
        WorkAttendance attendance = new WorkAttendance();
        Date clockIn = work.getClockIn(); //上班打卡时间
        Date date = new Date();
        // 转换为时间戳
        long timestamp1 = clockIn.getTime();
        long timestamp2 = date.getTime();
        // 计算时间差，单位毫秒
        long difference = timestamp2 - timestamp1;
        // 计算工作时长 将时间差转换为小时
        double hoursDifference = (double) difference / (1000 * 60 * 60);
        attendance.setId(work.getId()); // 主键id
        attendance.setLockOut(date);  // 下班打卡时间
        attendance.setWorkHours(Double.toString(hoursDifference)); // 工作时长
        int rows = baseMapper.updateWorkAttendance(attendance);

        return rows;
    }
}
