package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.BaseResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.domain.WorkAttendance;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.WorkAttendanceMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.YgSalaryBo;
import com.ruoyi.system.domain.vo.YgSalaryVo;
import com.ruoyi.system.domain.YgSalary;
import com.ruoyi.system.mapper.YgSalaryMapper;
import com.ruoyi.system.service.IYgSalaryService;

import java.math.BigDecimal;
import java.util.*;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2024-01-09
 */
@RequiredArgsConstructor
@Service
public class YgSalaryServiceImpl implements IYgSalaryService {

    private final YgSalaryMapper baseMapper;

    private final SysUserMapper sysUserMapper;

    private final SysPostMapper sysPostMapper;

    private final WorkAttendanceMapper workAttendanceMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public YgSalary queryById(Integer id){
        return baseMapper.selectSaralyById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<YgSalaryVo> queryPageList(YgSalaryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<YgSalary> lqw = buildQueryWrapper(bo);
        Page<YgSalaryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    @Override
    public BaseResult selectSalaryList(YgSalaryBo bo, PageQuery pageQuery) {
        BaseResult base = new BaseResult();
        Map<String,Object> params = new HashMap<String,Object>();
        Integer start = null;
        if (pageQuery.getPageNum() != null && pageQuery.getPageSize() != null && pageQuery.getPageNum() > 0) {
            start = pageQuery.getPageNum() != null ? (pageQuery.getPageNum() - 1) * pageQuery.getPageSize() : null;
            params.put("page", start);
            params.put("rows", pageQuery.getPageSize());
        }
        params.put("ygName",bo.getYgName());
        params.put("ygNumber",bo.getYgNumber());
        List<YgSalary> ygSalaries = baseMapper.selectSaralyList(params);
        for (YgSalary list:ygSalaries) {

//            SysUser sysUser = sysUserMapper.selectUserByUserName(list.getYgName());
            // 计算工龄
            Date createTime = list.getUserDate();
            Date nowDate = DateUtils.getNowDate();
            Long monthsBetween = DateUtils.getMonthsBetween(createTime, nowDate); // 工龄（以yue份为单位）
            list.setYgSeniority(monthsBetween); // 员工工龄
            // 根据打卡情况，来确定扣款
            // 根据用户编号查询该用户上一月打卡次数
           int mounthCount = workAttendanceMapper.selelectCount(list.getYgNumber());
            BigDecimal deduction = null; // 扣款
           if (mounthCount < 26) {
               // 计算扣除工资(扣除工资 = 基本工资/26*(26-mounthCount)
               BigDecimal basicSalary = list.getBasicSalary();
               BigDecimal divisor = new BigDecimal("26");
               // 进行除法运算
               BigDecimal result = basicSalary.divide(divisor, 10, BigDecimal.ROUND_HALF_UP);
               // 输出结果
               System.out.println("Result of division: " + result);
               Integer count = 26 - mounthCount;
               BigDecimal mult = new BigDecimal(count);
               deduction = result.multiply(mult).setScale(2, BigDecimal.ROUND_HALF_UP); // 扣款
               // 输出扣款结果
               System.out.println("扣款: " + deduction);

           } else if (mounthCount == 26) {
               // 扣款金额为0
               deduction = new BigDecimal("0.00");
           }
            list.setDeduction(deduction);
           // 确定实际工资 （基本工资+岗位津贴-扣款+工龄奖金）
            if (list.getPostAllowance() != null) {
                BigDecimal add = list.getBasicSalary().add(new BigDecimal(list.getPostAllowance()));
                BigDecimal subtract = add.subtract(deduction);
                BigDecimal realWages = null;  // 实际工资
                if (monthsBetween < 12) { // 工龄小于1年 -- 工龄奖金 300
                    list.setYgSenioiityMoney(new BigDecimal("300")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("300")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                } else if (monthsBetween >= 12 && monthsBetween < 36) { // 工龄大于1年小于3年 --工龄奖金 600
                    list.setYgSenioiityMoney(new BigDecimal("600")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("300")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                } else if (monthsBetween >= 36) { // 工龄大于3年 --工龄奖金 1000
                    list.setYgSenioiityMoney(new BigDecimal("1000")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("1000")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                }
                list.setRealWages(realWages);
            }else {
                BigDecimal add = list.getBasicSalary().add(new BigDecimal("0.00"));
                BigDecimal subtract = add.subtract(deduction);
                BigDecimal realWages = null;  // 实际工资
                if (monthsBetween < 12) { // 工龄小于1年 -- 工龄奖金 300
                    list.setYgSenioiityMoney(new BigDecimal("300")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("300")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                } else if (monthsBetween >= 12 && monthsBetween < 36) { // 工龄大于1年小于3年 --工龄奖金 600
                    list.setYgSenioiityMoney(new BigDecimal("600")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("300")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                } else if (monthsBetween >= 36) { // 工龄大于3年 --工龄奖金 1000
                    list.setYgSenioiityMoney(new BigDecimal("1000")); // 工龄奖金
                    realWages = subtract.add(new BigDecimal("1000")).setScale(2, BigDecimal.ROUND_HALF_UP); // 实际工资
                }
                list.setRealWages(realWages);
            }
        }
        int total = baseMapper.selectSaralyCount(params);
        base.setData(ygSalaries);
        base.setPageSize(pageQuery.getPageSize());
        base.setTotal(total);
        base.setSuccess(true);
        base.setCode(200);
        return base;
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<YgSalaryVo> queryList(YgSalaryBo bo) {
        LambdaQueryWrapper<YgSalary> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<YgSalary> buildQueryWrapper(YgSalaryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<YgSalary> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getYgNumber()), YgSalary::getYgNumber, bo.getYgNumber());
        lqw.like(StringUtils.isNotBlank(bo.getYgName()), YgSalary::getYgName, bo.getYgName());
        lqw.eq(StringUtils.isNotBlank(bo.getYgPostId()), YgSalary::getYgPostId, bo.getYgPostId());
        lqw.eq(bo.getUserId() != null, YgSalary::getUserId, bo.getUserId());
        lqw.eq(bo.getBasicSalary() != null, YgSalary::getBasicSalary, bo.getBasicSalary());
        lqw.eq(StringUtils.isNotBlank(bo.getPostAllowance()), YgSalary::getPostAllowance, bo.getPostAllowance());
        lqw.eq(bo.getYgSeniority() != null, YgSalary::getYgSeniority, bo.getYgSeniority());
        lqw.eq(bo.getDeduction() != null, YgSalary::getDeduction, bo.getDeduction());
        lqw.eq(bo.getRealWages() != null, YgSalary::getRealWages, bo.getRealWages());
        lqw.eq(bo.getStatus() != null, YgSalary::getStatus, bo.getStatus());
//        lqw.eq(StringUtils.isNotBlank(bo.getLoginIp()), YgSalary::getLoginIp, bo.getLoginIp());
//        lqw.eq(bo.getLoginDate() != null, YgSalary::getLoginDate, bo.getLoginDate());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public AjaxResult insertByBo(YgSalary ygSalary) {
        // 根据用户编号查询用户信息
        SysUser sysUser = sysUserMapper.selectUserByygNumber(ygSalary.getYgNumber());
        if (sysUser != null) {
            // 校验用户编号与姓名是否唯一
            if (!sysUser.getName().equals(ygSalary.getYgName())) {
                return AjaxResult.error(203,"新增薪资失败，输入此员工的编号和姓名不匹配");
            }
            // 根据用户编号查询薪资列表
            YgSalary ygSa = baseMapper.selectSaraly(ygSalary.getYgNumber(),ygSalary.getYgTime());
            if (ygSa != null && ygSa.getYgTime() != null && ygSa.getYgTime().equals(ygSalary.getYgTime())) {
                return AjaxResult.error(203,"新增薪资失败，此员工当月的薪资已存在");
            }
            YgSalary ygSalaryMap = new YgSalary();
            ygSalaryMap.setYgNumber(ygSalary.getYgNumber());
//            ygSalaryMap.setYgName(ygSalary.getYgName());
            ygSalaryMap.setUserId(sysUser.getUserId());
            ygSalaryMap.setBasicSalary(ygSalary.getBasicSalary());
            ygSalaryMap.setYgTime(ygSalary.getYgTime());
           // 查询该用户对应的岗位信息
           SysPost sysPost = sysPostMapper.selectPostsByYgNumber(ygSalary.getYgNumber());

           ygSalaryMap.setYgPostId(sysPost.getPostId().toString()); // 岗位id

//           ygSalaryMap.setPostAllowance(sysPost.getPostAllowance()); // 岗位津贴
            ygSalaryMap.setStatus("0");

            YgSalary ygData = BeanUtil.toBean(ygSalaryMap, YgSalary.class);
            validEntityBeforeSave(ygData);
            // 新增绩效数据
            boolean flag = baseMapper.insert(ygData) > 0;
            if (flag) {
                ygSalary.setId(ygData.getId());
            }
            return AjaxResult.success("新增成功");
       }
        return AjaxResult.error(203,"新增薪资失败，你输入的员工编号不存在");
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(YgSalaryBo bo) {
//        YgSalary update = BeanUtil.toBean(bo, YgSalary.class);
//        validEntityBeforeSave(update);
        return baseMapper.updateSaralyById(bo.getId(),bo.getYgNumber(),bo.getBasicSalary().toString(),bo.getStatus(),bo.getYgTime()) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(YgSalary entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
