package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.system.domain.WorkAttendance;
import com.ruoyi.system.domain.vo.WorkAttendanceVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface WorkAttendanceMapper extends BaseMapperPlus<WorkAttendanceMapper, WorkAttendance, WorkAttendanceVo> {


    // 根据用户用户编号查询该用户本月打卡次数
    int selelectCount(@Param("YgNumber")String YgNumber);

    // 新增
//    int insertWorkAttendance(WorkAttendance attendance);

    // 修改
    int updateWorkAttendance(WorkAttendance attendance);

    // 根据userId查询该用户当日有没有打卡
    WorkAttendance selectWorkDay(@Param("userId")Long userId);

}
