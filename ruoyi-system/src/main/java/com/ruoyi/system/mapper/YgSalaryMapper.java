package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.YgSalary;
import com.ruoyi.system.domain.vo.YgSalaryVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2024-01-09
 */
public interface YgSalaryMapper extends BaseMapperPlus<YgSalaryMapper, YgSalary, YgSalaryVo> {

    // 根据用户编号查询薪资信息
    YgSalary selectSaraly(@Param("YgNumber")String YgNumber,@Param("ygTime")String ygTime);

    // 查询薪资列表
    List<YgSalary> selectSaralyList(Map<String,Object> params);

    int selectSaralyCount(Map<String,Object> params);

    // 根据id查询薪资信息和员工姓名
    YgSalary selectSaralyById(@Param("id")Integer id);

    // 根据id修改薪资信息
    int updateSaralyById(@Param("id")Integer id,@Param("ygNumber")String ygNumber,@Param("basicSalary")String basicSalary,@Param("status")String status,@Param("ygTime")String ygTime);
}
